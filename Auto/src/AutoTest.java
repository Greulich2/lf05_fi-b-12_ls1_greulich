
public class AutoTest {
	
	public static void main(String[] args) {
		Auto a1;
		
		a1 = new Auto();	
		
		a1.setHersteller("VW");
		a1.setModel("Polo");
		System.out.println(a1.getHersteller());
		System.out.println(a1.getModel());
		
		Auto a2 = new Auto("BMW", "3er");
		
		System.out.println(a2.getHersteller());
		System.out.println(a2.getModel());
		
		System.out.println(a2.toString());
		System.out.println(a2);
	}
	
		

}
