import java.util.Scanner;

public class Kontrollstrukturen_AB_Schliefen_1 {

	public static void main(String[] args) {
		
		Scanner eingabe = new Scanner(System.in);
		// Aufgabe A 
		System.out.println("wie viele Zahlen m�chtest du miteinander addieren?");
		int anzahl = eingabe.nextInt();
		int summe = 0;
		for(int i = 1; i <= anzahl ;i++) {
			summe += i;
		}
		System.out.println("Die summe ist: "+ summe);
		// Aufgabe B
		System.out.println("wie viele Zahlen m�chtest miteinander (nur gerade Zahlen) addieren?");
		int anzahl2 = eingabe.nextInt();
		summe = 0;
		int addierer= 2;
		for(int i = 1; i<= anzahl2; i++) {						
			summe += addierer;	
			addierer += 2;			
		}
		System.out.println("Die summe f�r ist: "+ summe);
		// Aufgabe C
		System.out.println("wie viele Zahlen m�chtest du miteinander (nur ungerade Zahlen) addieren?");
		int anzahl3 = eingabe.nextInt();
		summe = 1;
		int addierer2= 3;
		for(int i = 1; i<= anzahl3; i++) {						
			summe += addierer2;	
			addierer2 += 2;	
	}
		System.out.println("Die Summe f�r C ist: "+ summe);
	}
}