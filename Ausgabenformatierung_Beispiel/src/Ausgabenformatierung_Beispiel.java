
public class Ausgabenformatierung_Beispiel {

	public static void main(String[] args) {
		// Ganzzahl
		//System.out.printf("|%+-10d%+10d|",123456, -123456);
		
		// Kommazahlen
		//System.out.printf("|%+-10.2f|",12.123456789);
		
		// Zeichenketten
		//System.out.printf("|%10.3s|","Max Mustermann");
		
		// �bung
		//System.out.printf("Name:%-10sAlter:%-8d   Gewicht:%10.2f\n", "Max",18, 80.50);
		
		// Aufgabe 2
		System.out.printf("%-5s=%-19s=%4d\n","0!","",1);
		System.out.printf("%-5s=%-19s=%4d\n","1!",1,1);
		System.out.printf("%-5s=%-19s=%4d\n","2!","1*2",2);
		System.out.printf("%-5s=%-19s=%4d\n","3!","1*2*3",6);
		System.out.printf("%-5s=%-19s=%4d\n","4!","1*2*3*4",24);
		System.out.printf("%-5s=%-19s=%4d\n","5!","1*2*3*4*5",120);
	

	}

}
