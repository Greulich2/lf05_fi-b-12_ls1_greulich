
public class Auto {
	private String hersteller;
	private String model;
	
	public Auto() {
		this.hersteller = "unbekannt";
		this.model = "unbekannt";


	}
	public Auto(String hersteller, String model) {
		this.hersteller = hersteller;
		this.model = model;
	}
	
	
	
	public String getHersteller() {
		return this.hersteller;
	}
	
	public void setHersteller(String hersteller) {
		this.hersteller = hersteller;

	}

	public String getModel() {
		return this.model;
	}

	public void setModel(String model) {
		this.model = model;
	}
	
	public String toString() {
		return this.hersteller + " " + this.model;
	}
	
}
